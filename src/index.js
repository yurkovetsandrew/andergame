import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Games from './Games';

ReactDOM.render(
  <React.StrictMode>
    <Games />
  </React.StrictMode>,
  document.getElementById('root')
);

