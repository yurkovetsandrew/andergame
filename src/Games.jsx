import React from 'react';
import PageBeforeSudoku from './PageBeforeSudoku';
import Home from './Home';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link,
  useRouteMatch
} from "react-router-dom";

class Games extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      byn: 0
    }
  };

  componentDidMount(){
    fetch('http://data.fixer.io/api/latest?access_key=3658edb159d41d668c0409948af5b31f&base=USD')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data)
        this.setState({byn: data.rates.BYN})
      });
  }

  render() {

    return(
      <div>
        <Router>
          <div className='cap'>
            <NavLink to="/">
              <a className='toHome'><i class="fab fa-autoprefixer"></i>nderGame </a>
            </NavLink>
            <NavLink to="/games" activeClassName="">
              <a className='buttonOnCap games-on-cap'>Games</a>
            </NavLink>
            <NavLink to="/premium" activeClassName="">
              <a className='buttonOnCap'>Premium</a>
            </NavLink>
            <p className='partition'>|</p>
            <a className='buttonOnCap'>Theme</a>
            <span className='conventor'>EUR: {this.state.byn}</span>
          </div>
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
            <Route path="/sudoku-settings">
              <PageBeforeSudoku/>
            </Route>
            <Route path="/premium">
            </Route>
          </Switch>
        </Router>
      </div>
    )
  }
}
export default Games;
