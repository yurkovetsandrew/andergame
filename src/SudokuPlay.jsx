import React from 'react';

class SudokuPlay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hiddenCells: 0,
      sudoku: null
    }
  };

  componentDidMount() {
    this.autoFill()
  }

  quantityOfHiddenCells = () =>{
    let numberOfHiddenCells = 0
    if(this.props.level == 'easy'){
      numberOfHiddenCells = 0.3
    }
    if(this.props.level == 'medium'){
      numberOfHiddenCells = 0.45
    }
    if(this.props.level == 'hard'){
      numberOfHiddenCells = 0.6
    }
    if(this.props.level == 'pro'){
      numberOfHiddenCells = 0.7
    }
    numberOfHiddenCells *= Math.pow(this.props.fieldSize, 2)
    this.setState({hiddenCells: Math.floor(numberOfHiddenCells)});
  }

  validInput = (i, j, sudoku) =>{
    let searchX = i - i % Math.sqrt(this.props.fieldSize)
    let searchY = j - j % Math.sqrt(this.props.fieldSize)
    for (let x = searchX; x < Math.sqrt(this.props.fieldSize) + searchX; x++){
      for (let y = searchY; y < Math.sqrt(this.props.fieldSize) + searchY; y++) {
        if (x != i && y !=j && sudoku[x][y] == sudoku[i][j]) {
          return false;
        }
      }
    }
     for(let x = 0; x < this.props.fieldSize; x++) {
      if(j != x && sudoku[i][x] === sudoku[i][j]) {
        return false;
      }
    }
    for(let y = 0; y < this.props.fieldSize; y++) {
      /*if(sudoku[y] == undefined || sudoku[y][j] == undefined){
        return true
      }*/
      if(i != y && sudoku[y][j] === sudoku[i][j]) {
        return false;
      }
    }
    // debugger
    return true
  }

  selectionI = (placeX, placeY, element, sudoku, meaningX, meaningY, i) =>{
    let mistake = meaningY.some(elem => elem == i)
//debugger
    if(mistake){
      //debugger
      if((i + 1) % Math.sqrt(this.props.fieldSize) > 0){
        i++
        //debugger
        i = this.selectionI(placeX, placeY, element, sudoku, meaningX, meaningY, i)
      }
      else{
        i = i - Math.sqrt(this.props.fieldSize) + 1
        i = this.selectionI(placeX, placeY, element, sudoku, meaningX, meaningY, i)
      }
    }
    //debugger
    return i
  }

  selectionJ = (placeX, placeY, element, sudoku, meaningX, meaningY, j) =>{
    let mistake = meaningX.some(elem => elem == j)
    if(mistake){
      if((j + 1) % Math.sqrt(this.props.fieldSize) > 0){
        j++
        j = this.selectionJ(placeX, placeY, element, sudoku, meaningX, meaningY, j)
      }
      else{
        j = j - Math.sqrt(this.props.fieldSize) + 1
        j = this.selectionJ(placeX, placeY, element, sudoku, meaningX, meaningY, j)
      }
    }
    return j
  }

  fill = (placeX, placeY, element, sudoku, meaningX, meaningY) =>{
    let i = 0
    let j = 0
    if(placeX == 1 && placeY == 1){
      i = Math.floor(placeX * Math.random()* Math.sqrt(this.props.fieldSize))
      j = Math.floor(placeY * Math.random()* Math.sqrt(this.props.fieldSize))
    }
    else {
      i = (placeX - 1) * Math.sqrt(this.props.fieldSize) + Math.floor(Math.random()* Math.sqrt(this.props.fieldSize)) // { 0..6 + 0..2 }
      j = (placeY - 1) * Math.sqrt(this.props.fieldSize) + Math.floor(Math.random()* Math.sqrt(this.props.fieldSize))
      i = this.selectionI(placeX, placeY, element, sudoku, meaningX, meaningY, i)
      j = this.selectionJ(placeX, placeY, element, sudoku, meaningX, meaningY, j)
    }
    if(sudoku[i][j] != 0){
      this.fill(placeX, placeY, element, sudoku, meaningX, meaningY)
    }
    sudoku[i][j] = element
    if(!this.validInput(i, j, sudoku)){
      sudoku[i][j] = 0;
      this.fill(placeX, placeY, element, sudoku, meaningX, meaningY)
    }
    else{
      meaningX.push(j)
      meaningY.push(i)
    }
    return sudoku
  }

  autoFill = () =>{
    let sudoku = []
    for(let i = 0; i < this.props.fieldSize; i++){
      sudoku[i] = []
      for(let j = 0; j < this.props.fieldSize; j++){
        sudoku[i][j] = 0
      }
    }
    for(let element = 1; element <=this.props.fieldSize; element++){
      let meaningX = []
      let meaningY = []
      for(let placeX = 1; placeX <= Math.sqrt(this.props.fieldSize); placeX++){
        for(let placeY = 1; placeY <= Math.sqrt(this.props.fieldSize); placeY++){
          //debugger
          sudoku = this.fill(placeX, placeY, element, sudoku, meaningX, meaningY);
        }
      }
    }
    this.setState({ sudoku })
  }

  print = () => {
    const { sudoku } = this.state;
    return(
      <table>
        {sudoku.map((row, indexRow) => {
          const rowNumber = indexRow + 1
          let highlightRow = rowNumber % Math.sqrt(this.props.fieldSize, 2)
          if(rowNumber === this.props.fieldSize) {
            highlightRow = true
          }
          return(
            <tr>
              {row.map((elementRow, indexColumn) => {
                const columnNumber = indexColumn + 1;
                let highlight = columnNumber % Math.sqrt(this.props.fieldSize, 2)
                if(columnNumber === this.props.fieldSize) {
                  highlight = true
                }
                return(
                  <td className={(!highlight ? 'highlight' : ' ') + ' ' + (!highlightRow ? 'highlightRow' : ' ')}>
                    <input type="text" value={elementRow == 0? '':elementRow} onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown} data-i={indexRow} data-j={indexColumn}/>
                  </td>
                )
              })}
            </tr>
          )
        })}
      </table>
    )
  }

  handleKeyDown = (e) =>{
    const i = parseInt(e.target.dataset.i)
    const j = parseInt(e.target.dataset.j)
    if (e.key === 'Enter'){
      document.activeElement.blur()
    }
    if (e.keyCode == '38'){ //down
      const element = document.querySelector(`[data-j="${j}"][data-i="${i - 1}"]`)
      if(element){
        element.focus()
      }
    }
    if (e.keyCode == '37'){ //left
      const element = document.querySelector(`[data-j="${j - 1}"][data-i="${i}"]`)
      if(element){
        element.focus()
      }
    }
    if (e.keyCode == '40'){ //up
      const element = document.querySelector(`[data-j="${j}"][data-i="${i + 1}"]`)
      if(element){
        element.focus()
      }
    }
    if (e.keyCode == '39'){ //right
      const element = document.querySelector(`[data-j="${j + 1}"][data-i="${i}"]`)
      if(element){
        element.focus()
      }
    }
  }

  handleChange = (e) =>{
    //debugger;
    //e.target.dataset
    const i = e.target.dataset.i
    const j = e.target.dataset.j
    const newSudoku = [...this.state.sudoku]
    newSudoku[i][j] = e.target.value
    this.setState({sudoku: newSudoku});
  }
  /*fill = (i, j, sudoku) =>{
    sudoku[i][j] = Math.floor(Math.random() * this.props.fieldSize) + 1
    if(!this.validInput(i, j, sudoku)){
      this.fill(i, j, sudoku)
    }
    return sudoku
  }
  autoFill = () =>{
    let sudoku = []
    for(let i = 0; i < this.props.fieldSize; i++){
      sudoku[i] = []
      for(let j = 0; j < this.props.fieldSize; j++){
        sudoku[i][j] = 0
      }
    }
    for(let i = 0; i < this.props.fieldSize; i++){
        for(let j = 0; j < this.props.fieldSize; j++){
        sudoku = this.fill(i, j, sudoku)
      }
    }
    return(
      <table>
        {sudoku.map((row, index) => {
          const rowNumber = index + 1
          let highlightRow = rowNumber % Math.sqrt(this.props.fieldSize, 2)
          if(rowNumber === this.props.fieldSize) {
            highlightRow = true
          }
          return(
            <tr>
              {row.map((elementRow, index) => {
                const columnNumber = index + 1;
                let highlight = columnNumber % Math.sqrt(this.props.fieldSize, 2)
                if(columnNumber === this.props.fieldSize) {
                  highlight = true
                }
                return(
                  <td className={(!highlight ? 'highlight' : ' ') + ' ' + (!highlightRow ? 'highlightRow' : ' ')}>{elementRow}</td>
                )
              })}
            </tr>
          )
        })}
      </table>
    )
  }*/

  render() {

    return(
      <div>
        <p>Field Size: {this.props.fieldSize}</p>
        <p>Level: {this.props.level}</p>
        <p>Hidden Cells: {this.state.hiddenCells}</p>
        {this.state.sudoku && this.print()}
      </div>
    )
  }
}
export default SudokuPlay;
