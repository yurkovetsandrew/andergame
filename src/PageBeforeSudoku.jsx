import React from 'react';
import SudokuPlay from './SudokuPlay';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link,
  useRouteMatch
} from "react-router-dom";
class PageBeforeSudoku extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      level: 'easy',
      fieldSize: 9
    };
  };

  changeLevel = (level) =>{
    this.setState({level: level});
  }
  changeFieldSize = (number) =>{
    this.setState({fieldSize: number});
  }

  render() {

    return(
      <Router>
        <div>
          <Switch>
            <Route path="/sudoku-settings">
              <Link to="/sudoku">
                <button className='aaaa'>Play</button>
              </Link>
              <h1>Sudoku</h1>
              <div className='sudokuMenu'>
                <ul>
                  <li><button className={this.state.fieldSize == 4 ? 'selected' : '' } onClick={() => {this.changeFieldSize(4)}}>4x4</button></li>
                  <li><button className={this.state.fieldSize == 9 ? 'selected' : '' } onClick={() => {this.changeFieldSize(9)}}>9x9</button></li>
                  <li><button className={this.state.fieldSize == 16 ? 'selected' : '' } onClick={() => {this.changeFieldSize(16)}}>16x16</button></li>
                </ul>
                <ul className='pop-upMenu'>
                  <li><button className={this.state.level == 'easy' ? 'selected' : '' } onClick={() => {this.changeLevel('easy')}}>easy</button></li>
                  <li><button className={this.state.level == 'medium' ? 'selected' : '' } onClick={() => {this.changeLevel('medium')}}>medium</button></li>
                  <li><button className={this.state.level == 'hard' ? 'selected' : '' } onClick={() => {this.changeLevel('hard')}}>hard</button></li>
                  <li><button className={this.state.level == 'pro' ? 'selected' : '' } onClick={() => {this.changeLevel('pro')}}>pro</button></li>
                </ul>
              </div>
              <p>Field Size: {this.state.fieldSize}</p>
              <p>Level: {this.state.level}</p>
              {this.state.hiddenCells?
              <Link to="/sudoku">
                <button>Play</button>
              </Link>:
              <div></div>}
            </Route>
            <Route path="/sudoku">
              <SudokuPlay level={this.state.level}
                          fieldSize={this.state.fieldSize}/>
            </Route>
          </Switch>
        </div>
      </Router>
    )
  }
}
export default PageBeforeSudoku;
