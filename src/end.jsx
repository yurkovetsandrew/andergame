import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link,
  useRouteMatch
} from "react-router-dom";

class end extends React.Component {
  constructor(props) {
    super(props);
  };
render() {
    return(
      <div>
        <div className='font5'>
          <div className='end'>
            <a className='toHome'><i class="fab fa-autoprefixer"></i>nderGame </a>
            <Link to='/about' className='links-end'>
              <span>About us</span>
            </Link>
            <Link to='/help'className='links-end'>
              <span>Help for the project</span>
            </Link>
            <Link to='/privacy' className='links-end'>
              <span>Privacy Policy</span>
            </Link>
            <div className='container-logo'>
              <div className='button-end telegram'>
                <i class="fab fa-telegram-plane end-logo"></i>
              </div>
              <div className='button-end instagram'>
                <i class="fab fa-instagram end-logo"></i>
              </div>
              <div className='button-end mail'>
                <i class="far fa-envelope end-logo"></i>
              </div>
            </div>
          </div>
          <p className='andrew'>@Andrew 2021</p>
        </div>
      </div>
    )
  }
}
export default end;
