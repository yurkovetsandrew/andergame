import React from 'react';
import End from './end';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link,
  useRouteMatch
} from "react-router-dom";
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [
        {sender: 'Boris', message: 'Terrible service, not recommend.', img: 2},
        {sender: 'Mike', message: 'Great, I like everything.', img: 1 },
        {sender: 'Kate', message: 'I play 24/7. Perfectly😃', img: 3 }
      ],
      senderName: '',
      message: ''
    }
  };

  handleChangeSender = (e) => {
    this.setState({senderName: e.target.value});
  }
  handleChangeMessage = (e) => {
    this.setState({message: e.target.value});
  }

  handleClick = () => {
    this.addUser(this.state.senderName, this.state.message)
    this.setState({senderName: '', message: ''})
  }

  addUser = (nameSender, messageInComment) => {
    if(nameSender === '' && messageInComment === ''){
      return
    }
    const newComments = [...this.state.comments]
    newComments.push({sender: nameSender, message: messageInComment, img: 1})
    newComments.shift()
    this.setState({comments: newComments})
  }

  render() {
    const comments = [...this.state.comments]
     const listOfComments = comments.map((comment, index) =>
      <div className='comments' key={index}>
        <div id='comment' className= {comment.img == 1?('indefined'):(comment.img == 2?('boris'):('kate'))}></div>
        <p className='paragrafInComment'>
          {comment.sender}:
        </p>
        <span className='spanInComment'>
          <i>{comment.message}</i>
        </span>
      </div>
    )
    return(
      <div>
        <div className='font'>
          <div className='textOnMainPage'>
            <p className='mostPopular'>Most popular</p>
            <h1>Don't know what to play?</h1>
            <p className='playOurGames'>Play our games.</p>
            <Link to="/games">
              <button className='toGames'>Games</button>
            </Link>
          </div>
          <div className='catalog'>
            <div className='catalogImg'>
              <Link to="/sudoku-settings">
                <div className='sudokuImg img'>
                  <div className='imgOnMain'>
                    <i class="far fa-play-circle"></i>
                  </div>
                </div>
              </Link>
              <Link to="/sapper">
                <div className='sapperImg img'>
                  <div className='imgOnMain'>
                    <i class="far fa-play-circle"></i>
                  </div>
                </div>
              </Link>
              <Link to="/seaWar">
                <div className='seaWarImg img'>
                  <div className='imgOnMain'>
                    <i class="far fa-play-circle"></i>
                  </div>
                </div>
              </Link>
              <Link to="/snake">
                <div className='snakeImg img'>
                  <div className='imgOnMain'>
                    <i class="far fa-play-circle"></i>
                  </div>
                </div>
              </Link>
              <Link to="/bomberman">
                <div className='bombermanImg img'>
                  <div className='imgOnMain'>
                    <i className="far fa-play-circle"></i>
                  </div>
                </div>
              </Link>
              <Link to="/worms">
                <div className='wormsImg img'>
                  <div className='imgOnMain'>
                    <i className="far fa-play-circle"></i>
                  </div>
                </div>
              </Link>
            </div>
          </div>
          <div className='zxc'><a href='#comments'><i class="fas fa-angle-double-down bounce"></i></a></div>
        </div>
        <div className='fontOnMain'></div>
        <div className='font2'>
          <div className='ticker'>
            <div className="animate-area"></div>
            <div className="animate-area2"></div>
          </div>
        </div>
        <div className='font3'  id='comments'>
          <h5>Comments</h5>
          {listOfComments}
        </div>
        <div className='font4'>
          <h3 className="whrite-comment">Write a comment:</h3>
          <p className='p-comment'>Your name:
            <input className='comment-input name' placeholder="Name" type="text" value={this.state.senderName} onChange={this.handleChangeSender}/>
          </p>
          <p className='p-comment'>Comment:
            <textarea className='comment-input comment' placeholder="Comment" type="text" value={this.state.message} onChange={this.handleChangeMessage}/>
          </p>
            <button className="send-comment" onClick={this.handleClick} disabled = {(this.state.senderName && this.state.message && this.state.message.length < 80 && this.state.senderName.length < 40)?(""):("disabled")}>Submit</button>
        </div>
        <End/>
      </div>
    )
  }
}
export default Home;
